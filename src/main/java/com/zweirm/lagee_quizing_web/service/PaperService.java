package com.zweirm.lagee_quizing_web.service;

import com.zweirm.lagee_quizing_web.domain.Paper;
import com.zweirm.lagee_quizing_web.repository.PaperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaperService {
    @Autowired
    private PaperRepository paperRepository;

    public Paper findPaperById(Long id) {
        return paperRepository.findPaperById(id);
    }
}
