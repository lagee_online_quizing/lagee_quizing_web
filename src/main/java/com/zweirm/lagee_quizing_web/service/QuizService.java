package com.zweirm.lagee_quizing_web.service;

import com.zweirm.lagee_quizing_web.domain.Quiz;
import com.zweirm.lagee_quizing_web.domain.StudentUser;
import com.zweirm.lagee_quizing_web.repository.QuizRepository;
import com.zweirm.lagee_quizing_web.repository.StudentUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuizService {
    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private StudentUserRepository studentUserRepository;

    public List<Quiz> findQuizByClazz() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = userDetails.getUsername();
        StudentUser studentUser = studentUserRepository.findStudentUserByEmail(email);
        return quizRepository.findQuizByClazzes(studentUser.getClazz());
    }

    public Quiz findQuizById(Long quizId) {
        return quizRepository.findQuizById(quizId);
    }
}
