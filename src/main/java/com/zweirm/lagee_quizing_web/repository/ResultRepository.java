package com.zweirm.lagee_quizing_web.repository;

import com.zweirm.lagee_quizing_web.domain.Quiz;
import com.zweirm.lagee_quizing_web.domain.Result;
import com.zweirm.lagee_quizing_web.domain.StudentUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultRepository extends JpaRepository<Result, Long> {
    Result findResultByStudentUserAndQuiz(StudentUser studentUser, Quiz quiz);
}
