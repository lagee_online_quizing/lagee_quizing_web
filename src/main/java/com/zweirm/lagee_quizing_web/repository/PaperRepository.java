package com.zweirm.lagee_quizing_web.repository;

import com.zweirm.lagee_quizing_web.domain.Paper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaperRepository extends JpaRepository<Paper, Long> {
    Paper findPaperById(Long id);
}
