package com.zweirm.lagee_quizing_web.repository;

import com.zweirm.lagee_quizing_web.domain.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
}
