package com.zweirm.lagee_quizing_web.repository;

import com.zweirm.lagee_quizing_web.domain.Clazz;
import com.zweirm.lagee_quizing_web.domain.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuizRepository extends JpaRepository<Quiz, Long> {
    List<Quiz> findQuizByClazzes(Clazz clazz);

    Quiz findQuizById(Long id);
}
