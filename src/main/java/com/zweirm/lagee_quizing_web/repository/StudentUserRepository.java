package com.zweirm.lagee_quizing_web.repository;

import com.zweirm.lagee_quizing_web.domain.StudentUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentUserRepository extends JpaRepository<StudentUser, Long> {
    StudentUser findStudentUserByEmail(String email);

    @Query("SELECT student_user.id FROM StudentUser student_user WHERE student_user.email = ?1")
    String findByEmail(String email);
}
