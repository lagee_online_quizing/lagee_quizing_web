package com.zweirm.lagee_quizing_web.repository;

import com.zweirm.lagee_quizing_web.domain.Clazz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClazzRepository extends JpaRepository<Clazz, Long> {
    Clazz findClazzByName(String clazz);
}
