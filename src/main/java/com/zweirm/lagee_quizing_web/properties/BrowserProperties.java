package com.zweirm.lagee_quizing_web.properties;

/**
 * 浏览器Properties
 */
public class BrowserProperties {
    private String loginPage = "/login.html";

    public String getLoginPage() {
        return loginPage;
    }

    public void setLoginPage(String loginPage) {
        this.loginPage = loginPage;
    }
}
