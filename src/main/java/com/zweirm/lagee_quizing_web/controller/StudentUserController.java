package com.zweirm.lagee_quizing_web.controller;

import com.zweirm.lagee_quizing_web.domain.StudentUser;
import com.zweirm.lagee_quizing_web.service.StudentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StudentUserController {
    @Autowired
    private StudentUserService studentUserService;

    @RequestMapping("/doRegister")
    public String register(String email, String password, String phone, String clazz, String name) {
        studentUserService.saveUser(email, password, phone, clazz, name);
        return "redirect:/login";
    }

    @RequestMapping("/checkUserExist")
    @ResponseBody
    public String checkUserExist(String email) {
        return studentUserService.findUserByEmail(email);
    }
}
