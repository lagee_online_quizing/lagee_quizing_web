package com.zweirm.lagee_quizing_web.controller;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController {
    // 获取session中User信息
    @GetMapping("/user")
    public Authentication user(Authentication user) {
        return user;
    }
}
