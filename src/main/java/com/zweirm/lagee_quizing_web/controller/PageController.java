package com.zweirm.lagee_quizing_web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageController {
    @RequestMapping("/backLogin")
    public String goBackLogin() {
        return "/backLogin";
    }

    @RequestMapping("/main")
    public String goMain() {
        return "/main";
    }

    @RequestMapping("/register")
    public String goRegister() {
        return "/register";
    }

    @RequestMapping("/login")
    public String goLogin() {
        return "/login";
    }

    @RequestMapping("/takeQuiz")
    public String goTakeQuiz() {
        return "/takeQuiz";
    }

    @RequestMapping("/showResultWithResult")
    public String goShowResultWithResult() {
        return "/showResultWithResult";
    }
}
