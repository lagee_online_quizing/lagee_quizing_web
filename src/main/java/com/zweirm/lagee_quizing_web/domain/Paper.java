package com.zweirm.lagee_quizing_web.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Paper {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String description;
    private String type;
    @OneToMany(mappedBy = "paper")
    private List<Question> questions;
    private Integer amount;
    private String status;

    public Paper() {
    }

    public Paper(String name, String description, String type, List<Question> questions, Integer amount, String status) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.questions = questions;
        this.amount = amount;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
