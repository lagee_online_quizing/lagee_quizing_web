package com.zweirm.lagee_quizing_web.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Clazz {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    @ManyToMany
    @JoinTable(name = "quiz_clazz", joinColumns = @JoinColumn(name = "clazz_id"), inverseJoinColumns = @JoinColumn(name = "quiz_id"))
    private List<Quiz> quizzes;
    @OneToMany(mappedBy = "clazz")
    private List<StudentUser> studentUsers;

    public Clazz() {
    }

    public Clazz(String name, List<Quiz> quizzes, List<StudentUser> studentUsers) {
        this.name = name;
        this.quizzes = quizzes;
        this.studentUsers = studentUsers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Quiz> getQuizzes() {
        return quizzes;
    }

    public void setQuizzes(List<Quiz> quizzes) {
        this.quizzes = quizzes;
    }

    public List<StudentUser> getStudentUsers() {
        return studentUsers;
    }

    public void setStudentUsers(List<StudentUser> studentUsers) {
        this.studentUsers = studentUsers;
    }
}
