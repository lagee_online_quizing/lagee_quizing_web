package com.zweirm.lagee_quizing_web.domain;

import javax.persistence.*;

@Entity
public class Result {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    @JoinTable(name = "student_user_result", joinColumns = @JoinColumn(name = "result_id"), inverseJoinColumns = @JoinColumn(name = "student_user_id"))
    private StudentUser studentUser;
    @ManyToOne
    @JoinTable(name = "quiz_result", joinColumns = @JoinColumn(name = "result_id"), inverseJoinColumns = @JoinColumn(name = "quiz_id"))
    private Quiz quiz;
    private String passRate;

    public Result() {
    }

    public Result(StudentUser studentUser, Quiz quiz, String passRate) {
        this.studentUser = studentUser;
        this.quiz = quiz;
        this.passRate = passRate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StudentUser getStudentUser() {
        return studentUser;
    }

    public void setStudentUser(StudentUser studentUser) {
        this.studentUser = studentUser;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public String getPassRate() {
        return passRate;
    }

    public void setPassRate(String passRate) {
        this.passRate = passRate;
    }
}
