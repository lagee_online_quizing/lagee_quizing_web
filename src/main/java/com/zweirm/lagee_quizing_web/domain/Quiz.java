package com.zweirm.lagee_quizing_web.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Quiz {
    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String type;
    @ManyToMany
    @JoinTable(name = "quiz_clazz", joinColumns = @JoinColumn(name = "quiz_id"), inverseJoinColumns = @JoinColumn(name = "clazz_id"))
    private List<Clazz> clazzes;
    @OneToOne
    @JoinColumn(name = "paper_id", referencedColumnName = "id")
    private Paper paper;
    @OneToMany(mappedBy = "quiz")
    private List<Result> results;
    private String status;

    public Quiz() {
    }

    public Quiz(String title, String type, List<Clazz> clazzes, Paper paper, List<Result> results, String status) {
        this.title = title;
        this.type = type;
        this.clazzes = clazzes;
        this.paper = paper;
        this.results = results;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Clazz> getClazzes() {
        return clazzes;
    }

    public void setClazzes(List<Clazz> clazzes) {
        this.clazzes = clazzes;
    }

    public Paper getPaper() {
        return paper;
    }

    public void setPaper(Paper paper) {
        this.paper = paper;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
