package com.zweirm.lagee_quizing_web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LageeQuizingWebApplicationTests {

    @Test
    public void contextLoads() {
    }

}
