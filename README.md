# 腊鸡答题系统前台

#### 介绍
此系统是一个简要的在线考试系统，可以提供给在校老师进行课堂测验、成绩管理等功能。  
前台提供给学生使用，最核心的功能为参加考试。


#### 软件架构
项目采用前后端不分离方案。  
页面显示采用 Thymeleaf + Bootstrap  
后台逻辑采用 Spring Boot + MySQL, Maven, Spring Data JPA (Hibernate), Spring Security  


#### 目录结构
```
./  
├── src/   
|   ├── main/  
|   |   ├── java/com/zweirm/lagee_quizing__web/  
|   |   |   ├── config/  
|   |   |   ├── controller/  
|   |   |   ├── domain/  
|   |   |   ├── handler/  
|   |   |   ├── properties/  
|   |   |   ├── repository/  
|   |   |   ├── service/  
|   |   |   ├── LageeQuizingWebApplication.java  
|   |   |   └── ServletInitiializer.java  
|   |   └── Resources/  
|   |       ├── static/  
|   |       |   ├── css/  
|   |       |   ├── fonts/  
|   |       |   ├── img/  
|   |       |   └── js/  
|   |       ├── templates/  
|   |       └── application.yml  
|   └── test/  
└── pom.xml
```


#### 安装教程

1.  导入项目
2.  手动创建数据库`lagee`
3.  运行项目，待自动生成数据库
4.  手动在`clazz`表中录入数据
5.  开始使用

#### 在线展示地址
https://oj.front.ahza.xin/

**注册测试账号时，请不要使用自己经常使用的密码，以免因为本系统不稳定而发生密码泄露。**

